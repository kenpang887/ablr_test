import React from 'react';
import { render } from '@testing-library/react';
import Entity from './entity';

// Fake data
const fake_data = require('../../data/page_1.json');
const fake_entity = fake_data.results[0];

test('Entity renders correctly', () => {
  const { container } = render(<Entity entity={fake_entity} />);
  expect(container.firstChild).toHaveClass('merchant-col');
});
