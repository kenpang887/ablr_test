import React, { Component } from 'react';
import './entity.css';
import { Col } from 'reactstrap';

class Entity extends Component<props> {
    render() {
        const {entity} = this.props;
        return (
            <Col xs={12} md={6} className="merchant-col">
                <img src={entity.photo} alt="background-img" className="merchant-background" />
                <div className="merchant-information-wrapper">
                    <div className="merchant-information">
                        <img src={entity.merchant.logo} alt="" className="mr-3 merchant-logo"/>
                        <div className="media-body">
                            <div className="merchant-title">{entity.title || ''}</div>
                            <div className="merchant-location">{entity.location || ''}</div>
                        </div>
                    </div>
                </div>
            </Col>          
        );
    }
}

export default Entity;
