import React, { Component } from 'react';
import './dashboard.css';
import { Row, Button } from 'reactstrap';
import Entity from '../entity/entity';

let jsonFiles = [
    require('../../data/page_1.json'),
    require('../../data/page_2.json'),
    require('../../data/page_3.json')
];

class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            entities: [],
            index: 0,
            showMore: true,
        };
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData = () => {
        const {entities, index} = this.state;
        // Fetching JSON Data
        try {
            const new_entities = jsonFiles[index];

            this.setState({
                entities: entities.concat(new_entities.results || []),
                showMore: new_entities.next !== null
            });

        } catch (error) {
            console.log('Error occured during load local json file.', error);
        }

    }

    loadMore = () => {
        this.setState({
            index: this.state.index + 1,
        }, () => {
            this.fetchData();
        })
    }

    render() {
        const {entities, showMore} = this.state;
        return (
            <div className="dashboard">
                <Row className="show-grid">
                    {
                        entities.map((entity, index) => {
                            return (
                                <Entity entity={entity} key={index} />
                            );
                        })
                    }
                </Row>
                
                <Button variant="outline-primary" className="btn-primary load-more-btn" onClick={this.loadMore}>
                {
                    showMore ? 'View More' : 'No more stores :('
                }
                </Button>
                
            </div>
        );
    }
}

export default Dashboard;
