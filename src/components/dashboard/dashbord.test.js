import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import Dashboard from './dashboard';

test('Dashboard renders correctly', () => {
  const { container } = render(<Dashboard />);
  expect(container.firstChild).toHaveClass('dashboard');
});

test('handle server error', async () => {
    const {findAllByAltText} = render(<Dashboard />);
    const prev_items = await findAllByAltText('background-img');
    const prev_count = prev_items.length;
    
    // Fire button click
    fireEvent.click(screen.getByText('View More'));
    
    const items = await findAllByAltText('background-img');
    const count = items.length;
    
    expect(count).toBeGreaterThan(prev_count);
})